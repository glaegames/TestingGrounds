// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

USTRUCT(BlueprintType)
struct FActorSpawnInfoStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int MinSpawn;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int MaxSpawn;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MinScale;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MaxScale;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float Radius;

	FActorSpawnInfoStruct()
	{
		MinSpawn = 1;
		MaxSpawn = 1;
		MinScale = 1;
		MaxScale = 1;
		Radius = 500;
	}
};

USTRUCT()
struct FSpawnPosition
{
	GENERATED_USTRUCT_BODY()

	FVector Location;
	float Rotation;
	float Scale;
};

class UActorPool;

UCLASS()
class TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UFUNCTION(BlueprintCallable, Category = "Spawning")
	void PlaceActor(TSubclassOf<AActor> Actor, FActorSpawnInfoStruct SpawnInfo);
	
	UFUNCTION(BlueprintCallable, Category = "Spawning")
	void PlaceAIPawn(TSubclassOf<APawn> Pawn, FActorSpawnInfoStruct SpawnInfo);

	UFUNCTION(BlueprintCallable, Category = "Pool")
	void SetPool(UActorPool* Pool);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MinExtent;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MaxExtent;

	UPROPERTY(EditDefaultsOnly, Category = "Navigation")
	FVector NavigationBoundsOffset;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UActorPool * Pool;

	AActor* NavMeshBoundsVolume;

	TArray<AActor*> GarbageCollection;

	// Return true if sphere intersects with something
	bool CanSpawnAtLocation(FVector Location, float Radius);

	bool FindEmptyLocation(FVector& OutLocation, float Radius);

	template<class T>
	void RandomlyPlaceActors(TSubclassOf<T> ToSpawn, FActorSpawnInfoStruct SpawnInfo);

	void PlaceActor(TSubclassOf<AActor> Actor, const FSpawnPosition& SpawnPosition);
	
	void PlaceActor(TSubclassOf<APawn> Pawn, const FSpawnPosition& SpawnPosition);

	void PositionNavMeshBoundsVolume();
	
};
